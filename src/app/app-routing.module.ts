import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { PagesLayoutComponent } from './layout/pages-layout/pages-layout.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { FleetComponent } from './pages/fleet/fleet.component';
import { StatisticsComponent } from './pages/statistics/statistics.component';
import { WorkComponent } from './pages/work/work.component';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        component: PagesLayoutComponent,
        children: [
          {
            path: 'dashboard',
            loadChildren: () =>
              import('./pages/dashboard/dashboard.module').then(
                (m) => m.DashboardModule
              ),
            component: DashboardComponent,
            data: {
              breadcrumb: 'Dashboard',
            },
          },
          { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
          { path: 'fleet', component: FleetComponent },
          { path: 'work', component: WorkComponent },
          {
            path: 'statistic',
            loadChildren: () =>
              import('./pages/statistics/statistics.module').then(
                (m) => m.StatisticsModule
              ),
            component: StatisticsComponent,
            data: {
              breadcrumb: 'Statistics',
            },
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
