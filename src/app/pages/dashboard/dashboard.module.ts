import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardContentComponent } from './dashboard-content/dashboard-content.component';

const routes: Route[] = [
  {
    path: ':id',
    component: DashboardContentComponent,
    data: { breadcrumb: 'Dashboard' },
  },
];

@NgModule({
  declarations: [DashboardContentComponent],
  imports: [SharedModule, RouterModule.forChild(routes)],
})
export class DashboardModule {}
