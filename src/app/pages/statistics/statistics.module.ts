import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { MyResearchComponent } from './procurement/my-research/my-research.component';
import { PerformanceIndicatorComponent } from './procurement/performance-indicator/performance-indicator.component';
import { ProcurementComponent } from './procurement/procurement.component';
import { ProjectComponent } from './project/project.component';

const routes: Route[] = [
  {
    path: 'procurement',
    component: ProcurementComponent,
    children: [
      {
        path: 'performance',
        component: PerformanceIndicatorComponent,
        data: { breadcrumb: 'Performance Indicator' },
      },
      {
        path: 'research',
        component: MyResearchComponent,
        data: { breadcrumb: 'Research' },
      },
      { path: '', pathMatch: 'full', redirectTo: 'research' },
    ],
    data: {
      breadcrumb: 'Procurement',
    },
  },
  {
    path: 'project',
    component: ProjectComponent,
    data: {
      breadcrumb: 'Project',
    },
  },
];

@NgModule({
  declarations: [MyResearchComponent, ProcurementComponent, ProjectComponent],
  imports: [SharedModule, RouterModule.forChild(routes)],
})
export class StatisticsModule {}
