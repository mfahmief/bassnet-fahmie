import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { SidebarNavComponent } from './components/sidebar-nav/sidebar-nav.component';
import {
  faBars,
  faBarsStaggered,
  faBell,
  faCalendarDay,
  faCartShopping,
  faChartArea,
  faChartColumn,
  faChartPie,
  faChartSimple,
  faCirclePlus,
  faClipboardCheck,
  faGauge,
  faGears,
  faHouse,
  faListCheck,
  faTableColumns,
} from '@fortawesome/free-solid-svg-icons';
import {
  FaIconLibrary,
  FontAwesomeModule,
} from '@fortawesome/angular-fontawesome';

import { fas } from '@fortawesome/free-solid-svg-icons';
import { faPenToSquare, far } from '@fortawesome/free-regular-svg-icons';
import { RouterModule } from '@angular/router';
import { SidebarMenuComponent } from './components/sidebar-menu/sidebar-menu.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';

@NgModule({
  declarations: [
    HeaderComponent,
    SidebarNavComponent,
    SidebarMenuComponent,
    BreadcrumbComponent,
  ],
  exports: [
    HeaderComponent,
    SidebarNavComponent,
    FontAwesomeModule,
    SidebarMenuComponent,
    BreadcrumbComponent,
  ],
  imports: [CommonModule, FontAwesomeModule, RouterModule],
})
export class SharedModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas, far);
    library.addIcons(
      faHouse,
      faBell,
      faGears,
      faGauge,
      faCalendarDay,
      faClipboardCheck,
      faPenToSquare,
      faCartShopping,
      faListCheck,
      faBars,
      faCirclePlus,
      faChartSimple,
      faChartPie,
      faChartArea,
      faBarsStaggered,
      faChartColumn
    );
  }
}
