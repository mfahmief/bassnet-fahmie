import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sidebar-nav',
  templateUrl: './sidebar-nav.component.html',
  styleUrls: ['./sidebar-nav.component.scss'],
})
export class SidebarNavComponent implements OnInit, OnDestroy {
  routerSubscription!: Subscription;
  navList: SidebarNavList[] = [];
  currentUrl!: string;

  constructor(public router: Router) {}

  ngOnInit(): void {
    this.getSidebarNav(this.router.url);
    this.routerSubscription = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.getSidebarNav(event.url);
      }
    });
  }

  ngOnDestroy(): void {
    this.routerSubscription.unsubscribe();
  }

  getSidebarNav(url: string) {
    const newUrl = url.split('/');
    this.navList = [];
    this.currentUrl = newUrl[1];
    switch (newUrl[1]) {
      case 'dashboard':
        this.navList = [
          {
            label: 'DASHBOARD',
            url: ['/dashboard'],
            icon: ['fas', 'gauge'],
          },
          {
            label: 'PLANNER',
            url: ['/planner'],
            icon: ['fas', 'calendar-days'],
          },
          {
            label: 'APPROVAL',
            url: ['/approval'],
            icon: ['fas', 'clipboard-check'],
          },
          { label: 'ALERT', url: ['alert'], icon: ['far', 'bell'] },
        ];

        break;
      case 'fleet':
        break;
      case 'work':
        break;
      case 'statistic':
        this.navList = [
          {
            label: 'PROCUREMENT',
            url: ['/statistic', 'procurement', 'research'],
            icon: ['fas', 'cart-shopping'],
          },
          {
            label: 'PROJECT',
            url: ['/statistic', 'project'],
            icon: ['fas', 'list-check'],
          },
        ];
        break;
    }
  }
}

interface SidebarNavList {
  label: string;
  url: string[];
  icon: IconProp;
}
