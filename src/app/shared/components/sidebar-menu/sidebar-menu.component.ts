import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
  styleUrls: ['./sidebar-menu.component.scss'],
})
export class SidebarMenuComponent implements OnInit {
  menuList!: SidebarMenuList;
  routerSubscription!: Subscription;
  constructor(private router: Router) {}

  ngOnInit(): void {
    this.getSidebarNav(this.router.url);
    this.routerSubscription = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.getSidebarNav(event.url);
      }
    });
  }

  ngOnDestroy(): void {
    this.routerSubscription.unsubscribe();
  }

  getSidebarNav(url: string) {
    const newUrl = url.split('/');
    console.log(newUrl);
    this.menuList = null!;
    switch (newUrl[1]) {
      case 'dashboard':
        this.menuList = {
          title: 'Dashboard',
          data: [
            {
              label: 'Dashboard 1',
              url: ['dashboard', '1'],
              icon: ['fas', 'chart-simple'],
            },
            {
              label: 'Dashboard 2',
              url: ['dashboard', '2'],
              icon: ['fas', 'chart-simple'],
            },
            {
              label: 'Dashboard 3',
              url: ['dashboard', '3'],
              icon: ['fas', 'chart-simple'],
            },
          ],
        };
        break;
      case 'fleet':
        break;
      case 'work':
        break;
      case 'statistic':
        if (newUrl[2] === 'procurement') {
          this.menuList = {
            title: 'Procurement',
            data: [
              {
                label: 'My Research',
                url: ['/statistic', 'procurement', 'research'],
                icon: ['fas', 'chart-line'],
              },
            ],
          };
        } else if (newUrl[2] === 'project') {
          this.menuList = {
            title: 'Project',
            data: [
              {
                label: 'Project Level 1',
                url: ['/statistic', 'project', '1'],
                icon: ['fas', 'chart-line'],
              },
            ],
          };
        }
        break;
    }
  }
}

interface SidebarMenuList {
  title: string;
  data: {
    label: string;
    childLabel?: string | string[];
    url: string[];
    icon: IconProp;
  }[];
}
