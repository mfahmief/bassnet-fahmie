import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { FleetComponent } from './pages/fleet/fleet.component';
import { WorkComponent } from './pages/work/work.component';
import { StatisticsComponent } from './pages/statistics/statistics.component';
import { PagesLayoutComponent } from './layout/pages-layout/pages-layout.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    MainLayoutComponent,
    FleetComponent,
    WorkComponent,
    StatisticsComponent,
    PagesLayoutComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, SharedModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
